package com.timy.livretdeprogres;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.timy.livretdeprogres.model.Competence;
import com.timy.livretdeprogres.model.Cycle;
import com.timy.livretdeprogres.model.Student;
import com.timy.livretdeprogres.model.Teacher;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private final static String TAG = MainActivity.class.getSimpleName();
    private static final String FILE_NAME_PREFIX = "JPEG_";
    private static final int REQUEST_TAKE_PHOTO = 1;
    public static final String FILE_SCHEME = "file:";
    public static final String FILE_NAME_EXTENSION = ".jpg";

    private String mCurrentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dispatchTakePictureIntent();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_reset_bdd) {
            Log.e(TAG, "onNavigationItemSelected: hello reset bdd");
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.deleteAll();
            realm.commitTransaction();
            realm.beginTransaction();
            Student student = new Student("Gregory", "Nguyen");
            Teacher teacher = new Teacher("Mme", "Mickey");

            Cycle cp = new Cycle("Cours Préparatoire");

            List<Competence> competences = new ArrayList<>();

            Competence ecrit = new Competence("ecrit");
            competences.add(ecrit);
            Competence oral = new Competence("oral");
            competences.add(oral);
            Competence sexAppeal = new Competence("sex appeal");
            competences.add(sexAppeal);
            cp.competences = new RealmList<>(competences.toArray(new Competence[competences.size()]));

            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Rachel", "Griffin");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Jacqueline", "Williamson");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Diane", "Palmer");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Sandra", "Webb");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Andrew", "Collins");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Fred", "Scott");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Donna", "Sanchez");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Larry", "Weaver");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("George", "Sanchez");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("John", "Simmons");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Stephanie", "Ferguson");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Kathryn", "Ryan");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Arthur", "Matthews");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Albert", "Burns");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Jennifer", "Ross");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Paul", "Russell");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Julia", "Henry");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("James", "Crawford");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Stephen", "Graham");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Janice", "Hanson");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Gloria", "Mitchell");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Chris", "Mitchell");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Sean", "Rodriguez");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);
            student = new Student("Sarah", "Mason");
            student.teacher = teacher;
            student.cycle = cp;
            realm.copyToRealmOrUpdate(student);


            realm.commitTransaction();

            Snackbar.make(findViewById(android.R.id.content), "Reset BDD OK", Snackbar.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Log.wtf(TAG, "dispatchTakePictureIntent: ", new Exception());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = FILE_NAME_PREFIX + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                FILE_NAME_EXTENSION,         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = FILE_SCHEME + image.getAbsolutePath();
        return image;
    }
}

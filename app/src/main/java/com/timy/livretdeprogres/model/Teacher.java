package com.timy.livretdeprogres.model;

import com.timy.livretdeprogres.utils.RealmUtils;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Teacher extends RealmObject {

    @PrimaryKey
    public int id;
    public String name;
    public String firstName;

    public Teacher() {
    }

    public Teacher(String name, String firstName) {
        this.id = RealmUtils.autoIncrement(this.getClass());
        this.name = name;
        this.firstName = firstName;
    }
}

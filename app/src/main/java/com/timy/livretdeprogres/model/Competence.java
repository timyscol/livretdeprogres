package com.timy.livretdeprogres.model;

import com.timy.livretdeprogres.utils.RealmUtils;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Competence extends RealmObject {
    @PrimaryKey
    public int id;
    public String name;

    public Competence() {
        this.id = RealmUtils.autoIncrement(this.getClass());
    }

    public Competence(String name) {
        this.id = RealmUtils.autoIncrement(this.getClass());
        this.name = name;
    }
}

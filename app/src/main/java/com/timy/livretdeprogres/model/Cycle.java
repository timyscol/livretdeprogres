package com.timy.livretdeprogres.model;

import com.timy.livretdeprogres.utils.RealmUtils;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Cycle extends RealmObject {
    @PrimaryKey
    public int id;
    public String name;
    public RealmList<Competence> competences = new RealmList<>();

    public Cycle() {
        this.id = RealmUtils.autoIncrement(this.getClass());
    }

    public Cycle(String name) {
        this.id = RealmUtils.autoIncrement(this.getClass());
        this.name = name;
    }
}

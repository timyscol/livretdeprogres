package com.timy.livretdeprogres.model;

import com.timy.livretdeprogres.utils.RealmUtils;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class StudentProgress extends RealmObject {
    @PrimaryKey
    public int id;
    public Competence competence;
    public Student student;

    public StudentProgress() {
        this.id = RealmUtils.autoIncrement(this.getClass());
    }
}

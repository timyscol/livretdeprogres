package com.timy.livretdeprogres.utils;

import io.realm.Realm;

public class RealmUtils {

    public static int autoIncrement(Class clazz) {
        Realm realm = Realm.getDefaultInstance();
        Number maxId = realm.where(clazz).max("id");
        int id;
        if (maxId != null) {
            id = maxId.intValue() + 1;
        } else {
            id = 0;
        }
        return id;
    }
}
